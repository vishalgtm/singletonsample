package com.example.demo.com.example.demo.service;

import com.example.demo.com.example.demo.model.Card;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SimpleService {
    @Autowired
    private List<Card> cardList;

    public List<Card> createCard(String a, String b) {
       if(!cardList.isEmpty())cardList.clear();

        cardList.add(new Card(a,b));
        return cardList;
    }
}
