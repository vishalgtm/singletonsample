package com.example.demo.com.example.demo.com.example.demo;

import com.example.demo.com.example.demo.model.Card;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class config {
    @Bean("cardList")
    @Scope(value= ConfigurableBeanFactory.SCOPE_SINGLETON)
    public List<Card> getInstance(){
        return new ArrayList<Card>();
    }
}
