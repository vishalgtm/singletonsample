package com.example.demo.com.example.demo.model;

public class Card {
    private String a;
    private String b;

    public Card() {
    }

    public Card(String a, String b) {
        this.a = a;
        this.b = b;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }
}
