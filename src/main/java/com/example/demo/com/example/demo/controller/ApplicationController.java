package com.example.demo.com.example.demo.controller;

import com.example.demo.com.example.demo.model.Card;
import com.example.demo.com.example.demo.service.SimpleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ApplicationController {
    @Autowired
    private SimpleService simpleService;

    @Autowired
    private List<Card> cardList;

    @RequestMapping("/test")
    public String test(){
        List<Card> testCard= simpleService.createCard("test","test");
        System.out.println(testCard.get(0).getA());
        System.out.println(testCard.get(0).getB());

        return String.valueOf(cardList.size())+" ";
    }





}
